<!DOCTYPE html>
<html>

<head>
    <link rel="shortcut icon" href="img/favicon.png" type="image/png">
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Inlab</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='style/all.css'>
    <link rel="stylesheet" type="text/css" href="slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
</head>

<body class="background__lines">
    <div class="background-color">

        <!-- HEADER -->
        <header class="header" id="header">
            <div class="header__container">
                <div class="header__inner">
                    <div class="header__icon wow fadeInLeft">
                        <a href="index"><img src="img/logo.svg" /></a>
                    </div>
                    <div class="header__nav" id="nav">
                        <span class="header__nav--mobile close" onclick="showMenu()"><img src="img/close.svg"></span>
                        <nav class="nav wow fadeInRight">
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link" href="#header">Главная.</a>
                            </li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link" href="#offer">Услуги.</a>
                            </li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link"
                                    href="#benefit">Преимущества.</a></li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link" href="#case">Кейсы.</a></li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link" href="#review">Отзывы.</a>
                            </li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link" href="#footer">Контакты.</a>
                            </li>
                        </nav>
                    </div>
                    <div class="header__menu wow fadeInRight" onclick="showMenu('show')">
                        <img class="header__icon" src="img/hamburger-white.svg">
                    </div>
                </div>
            </div>
        </header>

        <!-- MAIN SECTION -->
        <section class="main">
            <div class="main__container">
                <div class="main__inner">
                    <div class="main__content animated  bounce">
                        <h1>Разработка сайтов полного цикла.</h1>
                        <p>Закажи разработку сайта и получи продвижение <br> в поисковых системах Google Yandex
                            беслпатно
                        </p>
                        <button id="call">Заказать</button>
                    </div>
                    <div class="main__img">
                        <img src="img/image.png" />
                    </div>
                </div>
            </div>
        </section>

        <!-- OFFER SECTION -->
        <section class="offer" id="offer">
            <div class="container">
                <div class="section__name">
                    <h1>Услуги.</h1>
                </div>
                <div class="offer__inner">
                    <div class="offer__content">
                        <div class="offer__card">
                            <img src="img/offer1.svg" />
                            <a href="landing">
                                <div class="offer__card__content">
                                    <h2>Продающий лендинг</h2>
                                    <p>Закажите одностраничник, повышающий продажи компании в несколько раз</p>
                                </div>
                            </a>
                        </div>
                        <div class="offer__card--button">
                            <a href="landing">
                                <button>Подробнее</button>
                            </a>
                        </div>
                    </div>
                    <div class="offer__content">
                        <div class="offer__card">
                            <img src="img/offer2.svg" />
                            <a href="business">
                                <div class="offer__card__content">
                                    <h2>Сайт-визитка</h2>
                                    <p>Информационный раздел, галереи, перечнь услуг, карты проезда и контакты</p>
                                </div>
                            </a>
                        </div>
                        <div class="offer__card--button">
                            <a href="business">
                                <button>Подробнее</button>
                            </a>
                        </div>
                    </div>
                    <div class="offer__content">
                        <div class="offer__card">
                            <img src="img/offer3.svg" />
                            <a href="corporate">
                                <div class="offer__card__content">
                                    <h2>Корпоративный сайт</h2>
                                    <p>Маркетинговый инструмент, который необходим крупным компаниям</p>
                                </div>
                            </a>
                        </div>
                        <div class="offer__card--button">
                            <a href="corporate">
                                <button>Подробнее</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- BENEFIT SECTION -->
        <section class="benefit" id="benefit">
            <div class="container">
                <div class="section__name">
                    <h1>Преимущества.</h1>
                </div>
                <div class="benefit__inner">
                    <div class="benefit__content">
                        <img src="img/ipo.svg" />
                        <h2>Эффективность</h2>
                        <p> работаем по специально разработанным <br> под ваш проект KPI</p>
                    </div>
                    <div class="benefit__content">
                        <img src="img/profits.svg" />
                        <h2>Продвижение</h2>
                        <p> бесплатная настройка интернет продвижения в Google, Yandex</p>
                    </div>
                    <div class="benefit__content">
                        <img src="img/wallet.svg" />
                        <h2>Экономия</h2>
                        <p> цены на 20% <br> ниже рыночных</p>
                    </div>
                    <div class="benefit__content">
                        <img src="img/targeting.svg" />
                        <h2>Уникальность</h2>
                        <p> разработка проекта индивидуально <br> под каждого клиента</p>
                    </div>
                    <div class="benefit__content">
                        <img src="img/guarantee.svg" />
                        <h2>Гарантия</h2>
                        <p> результат измеряемых <br> по KPI или полный <br> возврат средств</p>
                    </div>
                </div>
            </div>
        </section>

        <!-- CASES -->
        <section class="case" id="case">
            <div class="container">
                <div class="section__name">
                    <h1>Кейс.</h1>
                </div>
                <div class="case__inner">
                    <div class="case__card">
                        <img src="img/case1.svg" />
                        <a href="http://ylpdelivery.kz">
                            <div class="case__card__content">
                                <h2>YLP Delivery</h2>
                                <p>Landing page для ведущей логистической компании</p>
                            </div>
                        </a>
                    </div>
                    <div class="case__card">
                        <img src="img/case2.svg" />
                        <a href="http://marlepon.kz">
                            <div class="case__card__content">
                                <h2>Marlepon</h2>
                                <p>Широкоформатная печать в Алматы</p>
                            </div>
                        </a>
                    </div>
                    <div class="case__card">
                        <img src="img/case3.svg" />
                        <a href="https://transcompany.kz">
                            <div class="case__card__content">
                                <h2>TransCompany</h2>
                                <p>Услуги по грузоперевозкам из г. Алматы до г. Костаная</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <!-- REVIEWS SECTION -->
        <section class="review" id="review">
            <div class="container">
                <div class="section__name">
                    <h1>Отзывы.</h1>
                </div>
            </div>
            <div class="review__inner">
                <div class="review__card">
                    <img src="img/vector1.svg" />
                    <div class="review__content">
                        <div class="review__card__head">
                            <img src="img/foto.svg" />
                            <div class="review__card--text">
                                <h1>Каргобай Таксоев</h1>
                                <p>20.08.2019</p>
                            </div>
                        </div>
                        <div class="review__card__content">
                            <p>
                                К созданию сайта для продвежения нашего бизнеса мы отнеслись со всей серьезностью. Некоторое время подбирали ответсвенного исполнителя, 								читали отзывы и, наконец обратились в "InLab". В своем выборе мы не разочаровались! Сайт получился красивым, ярким, интересным. 								Сам заказ был выполнен точно в срок. Благодарим "InLab" за такой превосходный фирменный сайт.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="review__card">
                    <img src="img/vector1.svg" />
                    <div class="review__content">
                        <div class="review__card__head">
                            <img src="img/foto.svg" />
                            <div class="review__card--text">
                                <h1>Каргобай Таксоев</h1>
                                <p>20.08.2019</p>
                            </div>
                        </div>
                        <div class="review__card__content">
                            <p>
                                Обратились в компанию "InLab" с целью создания интернет-магазина для нашего бутика. Менеджеры быстро приняли заказ, 											проконсультировали по всем интересующим вопросам. Сайт получился красивым и соответсвовал всем нашим требованиям и пожеланиям. 									Основным условием была простота в использовании для наших клиентов. Так оно и получилось. Благодарим компанию "InLab" за прекрасно 								   выполненную работу!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="review__card">
                    <img src="img/vector1.svg" />
                    <div class="review__content">
                        <div class="review__card__head">
                            <img src="img/foto.svg" />
                            <div class="review__card--text">
                                <h1>Каргобай Таксоев</h1>
                                <p>20.08.2019</p>
                            </div>
                        </div>
                        <div class="review__card__content">
                            <p>
                                В "InLab" мы заказывали разработку сайта и остались очень довольны работой компании. В первую очередь хотелось бы отметить высокую 								   скорость разработки и адекватные цены. Все работы были закончены вовремя, никаких претензий по качеству работы не было. Выражаем 								благодарность всей компании "InLab" и надеемся на продуктивную работу по продвежению и рекламе в будущем.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="review__card">
                    <img src="img/vector1.svg" />
                    <div class="review__content">
                        <div class="review__card__head">
                            <img src="img/foto.svg" />
                            <div class="review__card--text">
                                <h1>Каргобай Таксоев</h1>
                                <p>20.08.2019</p>
                            </div>
                        </div>
                        <div class="review__card__content">
                            <p>
                               Мы предъявляли весьма строгие требования и детальнейшие пожелания к коду и внешнему виду маркетплейса. Мы были приятно удивлены, 							   когда сотрудники "InLab" в полной мере реализовали наши пожелания, сделав это в быстрые сроки.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="review__card">
                    <img src="img/vector1.svg" />
                    <div class="review__content">
                        <div class="review__card__head">
                            <img src="img/foto.svg" />
                            <div class="review__card--text">
                                <h1>Каргобай Таксоев</h1>
                                <p>20.08.2019</p>
                            </div>
                        </div>
                        <div class="review__card__content">
                            <p>
                                Мы благодарны Вам за профессионализм, быстрые сроки реализации Интернет-проекта и нестандартный, творческий подход к решению задачи! 								 Также благодарим подразделение "InLab" за эффективное продвижение нашего ресурса в Интернете!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="review__card">
                    <img src="img/vector1.svg" />
                    <div class="review__content">
                        <div class="review__card__head">
                            <img src="img/foto.svg" />
                            <div class="review__card--text">
                                <h1>Каргобай Таксоев</h1>
                                <p>20.08.2019</p>
                            </div>
                        </div>
                        <div class="review__card__content">
                            <p>         
								Выражаем благодарность компании "InLab". Наш сайт преобразился, появилась мобильная версия, своевременно вносятся все изменения. Я 								  	довольна качеством работ, соблюдением сроков и вовлеченностью специалистов в процесс творческой работы.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- MODAL -->
        <?php include 'components/modal.php'; ?>

        <!-- FOOTER -->
        <?php include 'components/footer.php'; ?>

    </div>

    <script type="text/javascript" src="slick/slick.min.js"></script>
    <script type="text/javascript" src="js/reviewSlider.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/animate.js"></script>
    <script type="text/javascript" src="js/nav.js"></script>
    <script type="text/javascript" src="js/modal.js"></script>


</body>

</html>
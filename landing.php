<!DOCTYPE html>
<html lang="en">

<head>
	<link rel="shortcut icon" href="img/favicon.png" type="image/png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inlab</title>
    <link rel='stylesheet' type='text/css' media='screen' href='style/all.css'>
</head>

<body class="background__lines landing">
    <div class="background-color">
        <!-- HEADER -->
        <header class="header landing" id="header">
            <div class="header__container">
                <div class="header__inner">
                    <div class="header__icon">
                        <a href="index"><img src="img/landing-logo.svg" /></a>
                    </div>
                    <div class="header__nav" id="nav" >
                        <span class="header__nav--mobile close" onclick="showMenu()"><img src="img/close.svg"></span>
                        <nav class="nav" >
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link landing" href="#header">Главная.</a></li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link landing" href="#stages">Этапы.</a></li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link landing" href="#benefit">Составляющая.</a></li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link landing" href="#price">Цены.</a></li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link landing" href="#case">Кейсы.</a></li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link landing" href="#footer">Контакты.</a></li>
                        </nav>
                    </div>
                    <div class="header__menu" onclick="showMenu('show')">
                        <img class="header__icon" src="img/menu.svg">
                    </div>
                </div>
            </div>
        </header>

        <!-- MAIN SECTION -->
        <section class="main landing">
            <div class="main__container landing">
                <div class="main__inner">
                    <div class="main__content landing">
                        <h1>Разработка сайтов полного цикла.</h1>
                        <div class="main__content landing--button">
                            <button id="call" >Заказать</button>
							<a href="#stages"><button>Побробнее</button></a>
                        </div>
                    </div>
                    <div class="main__img">
                        <img src="img/landing-image.svg" />
                    </div>
                </div>
            </div>
        </section>

        <!-- STAGES SECTION -->
        <section class="stages" id="stages">
            <div class="container">
                <div class="section__name">
                    <h1>Этапы.</h1>
                </div>
                <div class="stages__inner">
                    <div class="stages__card">
                        <div class="stages__number line">
                            <h1>01.</h1>
                        </div>
                        <div class="stages__content">
                            <h1>Аналитика &nbsp; бизнеса</h1>
                            <p>Изучаем особенности, вашей сферы деятельности, для построения общей логики работы сайта.
                            </p>
                        </div>
                    </div>
                    <div class="stages__card">
                        <div class="stages__number line">
                            <h1>02.</h1>
                        </div>
                        <div class="stages__content">
                            <h1>Подписание договора</h1>
                            <p>Дорабатываем и утверждаем ТЗ и функционал сайта. После подписываем договор.</p>
                        </div>
                    </div>
                    <div class="stages__card">
                        <div class="stages__number line">
                            <h1>03.</h1>
                        </div>
                        <div class="stages__content">
                            <h1>Разработка прототипа</h1>
                            <p>Готовим макет, проектируем блоки. Далее разрабатываем структуру сайта.</p>
                        </div>
                    </div>
                    <div class="stages__card">
                        <div class="stages__number line">
                            <h1>04.</h1>
                        </div>
                        <div class="stages__content">
                            <h1>Дизайн и визуализация</h1>
                            <p>Разработка дизайна, визуализация отдельных блоков и элементов.</p>
                        </div>
                    </div>
                    <div class="stages__card">
                        <div class="stages__number lines">
                            <h1>05.</h1>
                        </div>
                        <div class="stages__content">
                            <h1>Техническая реализация</h1>
                            <p>Сборка проекта и программирование сервисов. Тест функционала и системы.</p>
                        </div>
                    </div>
                    <div class="stages__card">
                        <div class="stages__number">
                            <h1>06.</h1>
                        </div>
                        <div class="stages__content">
                            <h1>Завершение &nbsp; работ</h1>
                            <p>Тестирование, финальная отладка, сдача проекта.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- COMPONENT SECTION -->
        <section class="benefit landing" id="benefit">
            <div class="container">
                <div class="section__name">
                    <h1>Составляющая.</h1>
                </div>
            </div>
            <div class="header__container">
                <div class="benefit__inner">
                    <div class="benefit__inner">
                        <div class="benefit__content landing">
                            <img src="img/marketolog.svg" />
                            <h2>Маркетолог </h2>
                            <p> Разрабатывает общую концепцию, "упаковывает" Ваш бизнес </p>
                        </div>
                        <div class="benefit__content landing">
                            <img src="img/kopiraiter.svg" />
                            <h2>Копирайтер</h2>
                            <p>Cоздает заголовки, которые приводят посетителя к целевому действию.</p>
                        </div>
                        <div class="benefit__content landing">
                            <img src="img/b2b.svg" />
                            <h2>Менеджер-проекта</h2>
                            <p>Ход работы на всех этапах. Контактное лицо студии перед заказчиком.</p>
                        </div>
                        <div class="benefit__content landing">
                            <img src="img/responsive.svg" />
                            <h2>Веб-дизайнер</h2>
                            <p> Cоздает красивый, эффектный макет, вызывающий доверие</p>
                        </div>
                        <div class="benefit__content landing">
                            <img src="img/programming.svg" />
                            <h2>Фронт-енд</h2>
                            <p>Программист делает верстку, проводит тесты на мобильных устройствах</p>
                        </div>
                        <div class="benefit__content landing">
                            <img src="img/teamwork.svg" />
                            <h2>SMM-специалист</h2>
                            <p>Специалист по контекстной рекламе, проводит предварительный анализ</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- PRICE SECTION -->
        <section class="price" id="price">
            <div class="container">
                <div class="section__name">
                    <h1>Цена.</h1>
                </div>
                <div class="price__inner">
                    <div class="price__card">
                        <p>от <span>50 000</span></p>
                    </div>
                    <div class="price__card">
                        <h2>• Продающий дизайн</h2>
                        <h2>• Адаптивная верстка</h2>
                        <h2>• Продающие блоки</h2>
                        <h2>• Креативная анимация </h2>
                    </div>
                    <div class="price__card">
                        <h2>• Сроки разработки от 1 недели</h2>
                        <h2>• Настройка аналитики</h2>
                        <h2>• Регистрация в поисковых системах</h2>
                    </div>
                </div>
            </div>
        </section>

        <!-- CASES -->
        <section class="case" id="case">
            <div class="container">
                <div class="section__name">
                    <h1>Кейс.</h1>
                </div>
                <div class="case__inner">
                    <div class="case__card">
                        <img src="img/case1.svg" />
                        <div class="case__card__content landing">
                            <h2>YLP Delivery</h2>
                            <p>Landing page для ведущей логистической компании</p>
                        </div>
                    </div>
                    <div class="case__card">
                        <img src="img/case2.svg" />
                        <div class="case__card__content landing">
                            <h2>Marlepon</h2>
                            <p>Широкоформатная печать в Алматы</p>
                        </div>
                    </div>
                    <div class="case__card">
                        <img src="img/case3.svg" />
                        <div class="case__card__content landing">
                            <h2>TransCompany</h2>
                            <p>Услуги по грузоперевозкам из г. Алматы до г. Костаная</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- SUBMIT APPLICATION -->
		<?php include 'components/application.php'; ?>

        <!-- MODAL -->
        <?php include 'components/modal.php'; ?>

        <!-- FOOTER -->
        <?php include 'components/footer.php'; ?>

    </div>
    <script type="text/javascript" src="js/nav.js"></script>
    <script type="text/javascript" src="js/modal.js"></script>

</body>

</html>
$(document).ready(function () {    
    $(".review__inner").slick({
        slidesToShow: 4,
        autoplay: true,
        autoplaySpeed: 3500,
        arrows: false,
        centerMode: true,
        responsive: [
            {
                breakpoint: 1220,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 2,
                    centerMode: true
                }
            },
            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    centerMode:false

                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    centerMode:false
                }
            },
            {
                breakpoint:380,
                settings:{
                    slidesToShow:1,
                    centerMode:false
                }
            }
        ]
    })
})



        <section class="modal" id="modaladd">
            <div class="modal--backdrop" onclick="hideModal()"></div>
            <div class="modal-inner">
                <img src="img/close.svg" class="modal-close" onclick="hideModal()">
                <h1>Заказать обратный звонок</h1>
                <p>Оставьте заявку на расчет стоимости</p>
                <form class="modal-form" method="POST" action="callback.php">
                    <fieldset>
                        <input type="text" name="username" placeholder="Имя" required>
                    </fieldset>
                    <fieldset>
                        <input type="text" name="phone_number" placeholder="Ваш телефон" required>
                    </fieldset>
                    <fieldset>
                        <button>Оставить заявку</button>
                    </fieldset>
                </form>
            </div>
        </section>
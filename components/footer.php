<footer class="footer" id="footer">
            <div class="header__container footer">
                <div class="footer__inner">
                    <div class="footer__card">
                        <div class="footer__logo">
                            <a href="#header"><img src="img/footer-image.svg"></a>
                            <p>2019, ⓒ Inlab
                                <br> Все права защищены.</p>
                            <div class="footer__social">
                                <a><img src="img/facebook.svg"></a>
                                <a><img src="img/instagram.svg"></a>
                                <a><img src="img/vk.svg"></a>
                            </div>
                        </div>
                    </div>
                    <div class="footer__card">
                        <div class="footer__nav">
                            <div class="footer__name">
                                <h1>Компания.</h1>
                            </div>
                            <nav class="footer__nav">
                                <li class="nav__footer__item"><a class="nav__footer__link" href="index">Главная.</a>
                                </li>
                                <li class="nav__footer__item"><a class="nav__footer__link" href="#offer">Услуги.</a>
                                </li>
                                <li class="nav__footer__item"><a class="nav__footer__link" href="#benefit">Преимущества.</a></li>
                                <li class="nav__footer__item"><a class="nav__footer__link" href="#case">Кейсы.</a></li>
                                <li class="nav__footer__item"><a class="nav__footer__link" href="#review">Отзывы.</a>
                                </li>
                            </nav>
                        </div>
                    </div>
                    <div class="footer__card">
                        <div class="footer__offer">
                            <div class="footer__name">
                                <h1>Услуги.</h1>
                            </div>
                            <div class="footer__offer__nav">
                                <li class="nav__footer__item"><a href="landing" class="nav__footer__link">Продающий
                                        лендинг</a>
                                </li>
                                <li class="nav__footer__item"><a href="business" class="nav__footer__link">Сайт-визитка</a></li>
                                <li class="nav__footer__item"><a href="corporate" class="nav__footer__link">Корпоративный
                                        сайт</a>
                                </li>
                            </div>
                        </div>
                    </div>
                    <div class="footer__card">
                        <div class="footer__contacts">
                            <div class="footer__name">
                                <h1>Контакты.</h1>
                            </div>
                            <a href="tel:+7 701 351 77 18">+7 701 351 77 18</a>
                            <p>г. Алматы, ул. Панфилова 98</p>
                            <p>inlab@gmail.com</p>
                        </div>
                    </div>
                    <div class="footer__card">
                        <div class="footer__input">
                            <div class="footer__name">
                                <h1>Оставить заявку.</h1>
                            </div>
								<form  method="POST" action="callback.php">
									<fieldset>
										<input type="text" name="username" placeholder="Имя">
									</fieldset>
									<fieldset>
										<input type="tel" name="phone_number" placeholder="Телефон">
									</fieldset>
									<fieldset>
										<button>Отправить</button>
									</fieldset>	
								</form>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
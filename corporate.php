<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="shortcut icon" href="img/favicon.png" type="image/png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inlab</title>
    <link rel='stylesheet' type='text/css' media='screen' href='style/all.css'>

</head>

<body class="background__lines corporate">
    <div class="background-color corporate">
        <!-- HEADER -->
        <header class="header landing" id="header">
            <div class="header__container">
                <div class="header__inner">
                    <div class="header__icon">
                        <a href="index"><img src="img/landing-logo.svg" /></a>
                    </div>
                    <div class="header__nav" id="nav">
                        <span class="header__nav--mobile close" onclick="showMenu()"><img src="img/close.svg"></span>
                        <nav class="nav">
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link landing"
                                    href="#header">Главная.</a></li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link landing"
                                    href="#stages">Этапы.</a></li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link landing"
                                    href="#benefit">Составляющая.</a></li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link landing"
                                    href="#price">Цены.</a></li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link landing"
                                    href="#case">Кейсы.</a></li>
                            <li class="nav__item"><a onclick="showMenu()" class="nav__link landing"
                                    href="#footer">Контакты.</a></li>
                        </nav>
                    </div>
                    <div class="header__menu" onclick="showMenu('show')">
                        <img class="header__icon" src="img/menu.svg">
                    </div>
                </div>
            </div>
        </header>
        <!-- MAIN SECTION -->
        <section class="main">
            <div class="main__container corporate">
                <div class="main__inner corporate">
                    <div class="main__img corporate">
                        <img src="img/corporate-image.svg" />
                        <div class="main__content corporate">
                            <h1>Сайт-визитка <br>на ваш вкус!</h1>
                            <div class="main__content__corporate--button">
                                <button id="call">Заказать</button>
								<a href="#stages"><button>Побробнее</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="main__content corporate tablet">
                        <h1>Сайт-визитка <br>на ваш вкус!</h1>
                        <div class="main__content__corporate--button">
                            <button id="call1">Заказать</button>
							<a href="#stages"> <button>Побробнее</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- STAGES SECTION -->
        <section class="stages" id="stages">
            <div class="container">
                <div class="section__name">
                    <h1>Этапы.</h1>
                </div>
                <div class="stages__inner business">
                    <img src="img/business-stage.svg" alt="">
                </div>
            </div>
        </section>

        <!-- COMPONENT SECTION -->
        <section class="benefit landing" id="benefit">
            <div class="container">
                <div class="section__name">
                    <h1>Составляющая.</h1>
                </div>
            </div>
            <div class="header__container">
                <div class="benefit__inner">
                    <div class="benefit__inner">
                        <div class="benefit__content landing">
                            <img src="img/marketolog.svg" />
                            <h2>Маркетолог </h2>
                            <p> Разрабатывает общую концепцию, "упаковывает" Ваш бизнес </p>
                        </div>
                        <div class="benefit__content landing">
                            <img src="img/kopiraiter.svg" />
                            <h2>Копирайтер</h2>
                            <p>Cоздает заголовки, которые приводят посетителя к целевому действию.</p>
                        </div>
                        <div class="benefit__content landing">
                            <img src="img/b2b.svg" />
                            <h2>Менеджер-проекта</h2>
                            <p>Ход работы на всех этапах. Контактное лицо студии перед заказчиком.</p>
                        </div>
                        <div class="benefit__content landing">
                            <img src="img/responsive.svg" />
                            <h2>Веб-дизайнер</h2>
                            <p> Cоздает красивый, эффектный макет, вызывающий доверие</p>
                        </div>
                        <div class="benefit__content landing">
                            <img src="img/programming.svg" />
                            <h2>Фронт-енд</h2>
                            <p>Программист делает верстку, проводит тесты на мобильных устройствах</p>
                        </div>
                        <div class="benefit__content landing">
                            <img src="img/teamwork.svg" />
                            <h2>SMM-специалист</h2>
                            <p>Специалист по контекстной рекламе, проводит предварительный анализ</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- PRICE SECTION -->
        <section class="price" id="price">
            <div class="container">
                <div class="section__name">
                    <h1>Цена.</h1>
                </div>
                <div class="price__inner">
                    <div class="price__card corporate">
                        <p>от <span>200 000</span></p>
                    </div>
                    <div class="price__card corporate">
                        <h2>• Уникальный дизайн</h2>
                        <h2>• Проектирование карточки товара/услуги</h2>
                        <h2>• Интеграция с 1С и CRM системами</h2>
                        <h2>• Форма заказа и обратного звонка</h2>
                        <h2>• Счетчики статистики, карта, соц. Сети</h2>
                        <h2>• Использование новейших технологии</h2>
                    </div>
                </div>
            </div>
        </section>

        <!-- CASES -->
        <section class="case" id="case">
            <div class="container">
                <div class="section__name">
                    <h1>Кейс.</h1>
                </div>
                <div class="case__inner">
                    <div class="case__card">
                        <img src="img/case1.svg" />
                        <div class="case__card__content landing">
                            <h2>YLP Delivery</h2>
                            <p>Landing page для ведущей логистической компании</p>
                        </div>
                    </div>
                    <div class="case__card">
                        <img src="img/case2.svg" />
                        <div class="case__card__content landing">
                            <h2>Marlepon</h2>
                            <p>Широкоформатная печать в Алматы</p>
                        </div>
                    </div>
                    <div class="case__card">
                        <img src="img/case3.svg" />
                        <div class="case__card__content landing">
                            <h2>TransCompany</h2>
                            <p>Услуги по грузоперевозкам из г. Алматы до г. Костаная</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- SUBMIT APPLICATION -->
		<?php include 'components/application.php'; ?>

        <!-- MODAL -->
        <?php include 'components/modal.php'; ?>

        <!-- FOOTER -->
        <?php include 'components/footer.php'; ?>



    </div>
</body>
<script type="text/javascript" src="js/nav.js"></script>
<script type="text/javascript" src="js/modalc.js"></script>



</html>